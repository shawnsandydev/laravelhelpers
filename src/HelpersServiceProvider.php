<?php

namespace ShawnSandy\Helpers;

use Illuminate\Support\ServiceProvider;

class HelpersServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__.'/BaseHelpers.php';
        require_once __DIR__.'/Media.php';
        require_once __DIR__.'/Media.php';
    }
}